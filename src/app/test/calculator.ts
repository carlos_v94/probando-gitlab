export class Calculator {
    upperLimit: number;
    lowerLimit: number;

    constructor(minValue: number, maxValue: number) {
        this.lowerLimit = minValue;
        this.upperLimit = maxValue;
    }

    public add(number1: number, number2: number): number {
        this.validateArgs(number1, number2);

        const result = number1 + number2;

        if (result > this.upperLimit) {
            throw new Error('Upper limit exceeded');
        }

        return result;
    }

    public substract(number1: number, number2: number): number {
        this.validateArgs(number1, number2);

        const result = number1 - number2;

        if (result < this.lowerLimit) {
            throw new Error('Lower limit exceeded');
        }

        return result;
    }

    private validateArgs(arg1: number, arg2: number): boolean {
        if (arg1 > this.upperLimit) {
            throw new Error('First argument exceeds upper limit');
        }

        if (arg2 > this.upperLimit) {
            throw new Error('Second argument exceeds upper limit');
        }

        if (arg1 < this.lowerLimit) {
            throw new Error('First argument exceeds lower limit');
        }

        if (arg2 < this.lowerLimit) {
            throw new Error('Second argument exceeds lower limit');
        }

        return true;
    }
}
