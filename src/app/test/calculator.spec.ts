import { Calculator } from './calculator';

describe('Test for Calculator', () => {
    let calculator;

    beforeEach(() => {
        // Arrange (Preparar)
        calculator = new Calculator(-100, 100);
    });

    describe('test for add method', () => {
        it('2 + 2 = 4', () => {
            // Act (Actuar)
            const result = calculator.add(2, 2);

            // Assert (Afirmar)
            expect(result).toEqual(4);
        });

        it('4 + 7 = 11', () => {
            const result = calculator.add(4, 7);

            expect(result).toEqual(11);
        });
    });

    describe('test for substract method', () => {
        it('5 - 3 = 2', () => {
            const result = calculator.substract(5, 3);

            expect(result).toEqual(2);
        });

        it('substranct returning negative', () => {
            const result = calculator.substract(3, 5);

            expect(result).toEqual(-2);
        });
    });

    describe('test for limits', () => {
        it('get limits', () => {
            expect(calculator.upperLimit).toEqual(100);
            expect(calculator.lowerLimit).toEqual(-100);
        });

        it('add exceding upper limit', () => {
            try {
                expect(() => {
                    calculator.add(10, calculator.upperLimit);
                }).toThrowError('Upper limit exceeded');
            } catch (err) {
                // OK, the SUT works expected
            }
        });

        it('substract exceding lower limit', () => {
            try {
                expect(() => {
                    calculator.substract(calculator.lowerLimit, 10);
                }).toThrowError('Lower limit exceeded');
            } catch (err) {
                // OK, the SUT works as expected
            }
        });

        it('arguments exceed upper limit on add', () => {
            try {
                expect(() => {
                    calculator.add(calculator.upperLimit + 1, 10);
                }).toThrowError('First argument exceeds upper limit');

                expect(() => {
                    calculator.add(10, calculator.upperLimit + 1);
                }).toThrowError('Second argument exceeds upper limit');
            } catch (err) {
                // OK, the SUT works expected
            }
        });

        it('arguments exceed lower limit on add', () => {
            try {
                expect(() => {
                    calculator.add(calculator.lowerLimit - 1, 10);
                }).toThrowError('First argument exceeds lower limit');

                expect(() => {
                    calculator.add(10, calculator.lowerLimit - 1);
                }).toThrowError('Second argument exceeds lower limit');
            } catch (err) {
                // OK, the SUT works expected
            }
        });

        it('arguments exceed upper limit on substract', () => {
            try {
                expect(() => {
                    calculator.substract(calculator.upperLimit + 1, 10);
                }).toThrowError('First argument exceeds upper limit');

                expect(() => {
                    calculator.substract(10, calculator.upperLimit + 1);
                }).toThrowError('Second argument exceeds upper limit');
            } catch (err) {
                // OK, the SUT works expected
            }
        });

        it('arguments exceed lower limit on substract', () => {
            try {
                expect(() => {
                    calculator.substract(calculator.lowerLimit - 1, 10);
                }).toThrowError('First argument exceeds lower limit');

                expect(() => {
                    calculator.substract(10, calculator.lowerLimit);
                }).toThrowError('Second argument exceeds lower limit');
            } catch (err) {
                // OK, the SUT works expected
            }
        });
    });
});
